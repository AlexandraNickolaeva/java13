package dz3.part1;
//[JAVA13][ДЗ№3_1] ПЕРЕДЕЛАННАЯ Задача 4 от NIKOLAEVA-AD259
public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;

    TimeUnit(int hours) {
        if (hours < 24)
            this.hours = hours;
        else
            this.hours = hours % 24;
    }

    TimeUnit(int hours, int minutes) {
        this(hours);
        if (minutes < 60)
            this.minutes = minutes;
        else
            this.minutes = minutes % 60;
    }

    TimeUnit(int hours, int minutes, int seconds) {
        this(hours, minutes);
        if (seconds < 60)
            this.seconds = seconds;
        else
            this.seconds = seconds % 60;
    }

    public void screenTime() {
        System.out.printf("%02d:%02d:%02d\n", hours, minutes, seconds);
    }

    public void twelveFormat() {
        if (hours == 0) {
            System.out.printf("%02d:%02d:%02d am", 12, minutes, seconds);
        } else if (hours < 12)
            System.out.printf("%02d:%02d:%02d am", hours, minutes, seconds);
        else if (hours == 12)
            System.out.printf("%02d:%02d:%02d pm", hours, minutes, seconds);
        else {
            System.out.printf("%02d:%02d:%02d pm", hours - 12, minutes, seconds);
        }
        System.out.println(); // для читабельности вывода
    }

    public void addTime(int hours, int minutes, int seconds) {
        if ((this.hours + hours) < 24)
            this.hours = this.hours + hours;
        else
            this.hours = (this.hours + hours) % 24;
        if ((this.minutes + minutes) < 60)
            this.minutes = this.minutes + minutes;
        else {
            this.minutes = (this.minutes + minutes) % 60;
            this.hours++;
        }
        if ((this.seconds + seconds) < 60)
            this.seconds = this.seconds + seconds;
        else {
            this.seconds = (this.seconds + seconds) % 60;
            this.minutes++;
        }
    }

    public static void main(String[] args) {
        TimeUnit time1 = new TimeUnit(15, 59, 59);
        TimeUnit time2 = new TimeUnit(22, 10);
        TimeUnit time3 = new TimeUnit(2);
        // вывести на экран установленное в классе время:
        time1.screenTime();
        time2.screenTime();
        time3.screenTime();
        // вывести в классе время в 12-часовом формате:
        time1.twelveFormat();
        time2.twelveFormat();
        time3.twelveFormat();
        // метод, который прибавляет переданное время к установленному:
        time1.addTime(4, 55, 7);
        time2.addTime(2, 50, 0);
        time3.addTime(20, 5, 1);
        // проверяем как сработал метод addTime:
        time1.screenTime();
        time2.screenTime();
        time3.screenTime();
    }
}
