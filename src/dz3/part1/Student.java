package dz3.part1;
//[JAVA13][ДЗ№3_1] ПЕРЕДЕЛАННАЯ Задача 2 от NIKOLAEVA-AD259
public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String newSurname) {
        surname = newSurname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] newGrades) {
        grades = newGrades;
    }

    public void addGrades(int add) {
        for (int i = 1; i < grades.length; i++) {
            grades[i - 1] = grades[i];
        }
        grades[grades.length - 1] = add;
    }

    public double avgGrades() {
        int count = 0;
        double sum = 0;
        for (int i = 0; i < grades.length; i++) {
            sum += grades[i];
            if (grades[i] != 0)
                count++;
        }
        return sum / count;
    }
}
