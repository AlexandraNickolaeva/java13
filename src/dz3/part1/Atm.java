package dz3.part1;
//[JAVA13][ДЗ№3_1] ПЕРЕДЕЛАННАЯ Задача 8 от NIKOLAEVA-AD259
public class Atm {
   private double currency;
   private static int count;

    Atm(double currency) {
        this.currency = currency;
        count++;
    }

    public double dollarsToRubles(double dollars) {
        return dollars*currency;
    }

    public double roublesToDollars(double roubles) {
        return roubles/currency;
    }

    public static int count(){
        return count;
    }

    public static void main(String[] args) {
        Atm test1 = new Atm(60); // курс доллара
        System.out.println(test1.roublesToDollars(60.0)); // рубль -> доллар
        System.out.println(test1.dollarsToRubles(1)); //  доллар -> рубль

        System.out.println(Atm.count()); // кол-во созданных объектов

        Atm test2 = new Atm(60);
        System.out.println(test2.dollarsToRubles(2.0)); // доллар -> рубль

        Atm test3 = new Atm(60);

        System.out.println(Atm.count()); // кол-во созданных объектов
    }
}
