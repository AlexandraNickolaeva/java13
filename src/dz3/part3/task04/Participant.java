package dz3.part3.task04;

public class Participant {
    private String nameParticipant;

    public String getNameParticipant() {
        return nameParticipant;
    }

    public void setNameParticipant(String newNameParticipant) {
        this.nameParticipant = newNameParticipant;
    }

    @Override
    public String toString() {
        return nameParticipant;
    }
}