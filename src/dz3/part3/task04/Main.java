package dz3.part3.task04;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.util.Collections.reverse;
import static java.util.Collections.sort;

public class Main {

    public static void main(String[] args) {
        List<Participant> participants = new ArrayList<>();
        List<Dog> dogs = new ArrayList<>();
        List<Double> gradesAvgDogs = new ArrayList<>();
        List<Double> gradesAll = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt(); // кол-во участников

        for (int i = 0; i < count; i++) {
            participants.add(new Participant());
            participants.get(i).setNameParticipant(scanner.next());
        }

        for (int i = 0; i < count; i++) {
            dogs.add(new Dog());
            dogs.get(i).setNameDog(scanner.next());
        }

        double sum = 0;
        for (int i = 0, j = 0; i < count * 3; i++) {
            gradesAll.add(scanner.nextDouble());
            sum += gradesAll.get(i);
            if (gradesAll.size() % 3 == 0) {
                dogs.get(j).setGradeAvgDog((int) (sum / 3 * 10) / 10.0);
                gradesAvgDogs.add(dogs.get(j).getGradeAvgDog());
                sum = 0;
                j++;
            }
        }
        winners(count, dogs, gradesAvgDogs, participants);
    }

    public static void winners(int count, List<Dog> dogs, List<Double> gradesAvgDogs, List<Participant> participants) {
        sort(gradesAvgDogs);
        reverse(gradesAvgDogs);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < count; j++) {
                if (gradesAvgDogs.get(i).equals(dogs.get(j).getGradeAvgDog()))
                    System.out.println(participants.get(j) + ": " + dogs.get(j) + ", " + dogs.get(j).getGradeAvgDog());
            }
        }
    }
}