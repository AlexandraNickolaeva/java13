package dz3.part3.task04;

public class Dog {
    private String nameDog;
    private double gradeAvgDog;

    public String getNameDog() {
        return nameDog;
    }

    public void setNameDog(String newNameDog) {
        this.nameDog = newNameDog;
    }

    public double getGradeAvgDog() {
        return gradeAvgDog;
    }

    public void setGradeAvgDog(double newGradeAvgDog) {
        this.gradeAvgDog = newGradeAvgDog;
    }

    @Override
    public String toString() {
        return nameDog;
    }
}