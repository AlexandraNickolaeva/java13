package dz3.part3.task02;

public class Main{
    public static void main(String[] args) {
        Stool stool = new Stool();
        Table table = new Table();
        BestCarpenterEver check = new BestCarpenterEver();
        System.out.println(check.check(stool));
        System.out.println(check.check(table));
    }
}
