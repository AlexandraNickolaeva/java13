package dz3.part3.task02;

public class BestCarpenterEver {
    public boolean check(Furniture furniture) {
        return furniture instanceof Stool;
    }
}
