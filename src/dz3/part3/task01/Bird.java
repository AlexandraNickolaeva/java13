package dz3.part3.task01;

class Bird extends Animals implements WayOfBirth {
    public void kind() {
        System.out.println("Птицы");
    }

    @Override
    public void wayOfBirth() {
        System.out.println("Откладывают яйца");
    }
}

