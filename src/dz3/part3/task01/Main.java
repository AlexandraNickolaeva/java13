package dz3.part3.task01;

public class Main {
    public static void main(String[] args) {

        Bat bat = new Bat();
        bat.eat();
        bat.sleep();
        bat.kind();
        bat.wayOfBirth();
        bat.flying();
        bat.fastOrSlow();

        Dolphin dolphin = new Dolphin();
        dolphin.eat();
        dolphin.sleep();
        dolphin.kind();
        dolphin.wayOfBirth();
        dolphin.swimming();
        dolphin.fastOrSlow();

        Eagle eagle = new Eagle();
        eagle.eat();
        eagle.sleep();
        eagle.kind();
        eagle.wayOfBirth();
        eagle.flying();
        eagle.fastOrSlow();

        GoldFish goldFish = new GoldFish();
        goldFish.eat();
        goldFish.sleep();
        goldFish.kind();
        goldFish.wayOfBirth();
        goldFish.swimming();
        goldFish.fastOrSlow();
    }
}
