package dz3.part3.task01;

public abstract class Animals {

    public final void eat() {
        System.out.println("Eat");
    }

    public final void sleep() {
        System.out.println("Sleep");
    }
}

