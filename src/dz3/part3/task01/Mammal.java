package dz3.part3.task01;

class Mammal extends Animals implements WayOfBirth {
    public void kind() {
        System.out.println("Млекопитающее");
    }

    @Override
    public void wayOfBirth() {
        System.out.println("Живородящие");
    }
}

