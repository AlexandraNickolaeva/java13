package dz3.part3.task03;

import java.util.ArrayList;
import java.util.Scanner;

public class Matrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] array = new int[m][n];
        ArrayList<Integer> list = new ArrayList<>();
        int k = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                list.add(k, i + j);
                System.out.print(list.get(k) + " ");
                k++;
            }
            System.out.println();
        }
    }
}
