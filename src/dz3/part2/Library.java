package dz3.part2;

import java.util.ArrayList;

public class Library {
    ArrayList<Book> books = new ArrayList<>();

    public void addBook(Book newBook) {
        int k = 0;
        if (books.size() == 0)
            books.add(newBook);
        else {
            for (int i = 0; i < books.size(); i++) {
                if (books.get(i).getNameBook().equals(newBook.getNameBook())) {
                    k = 1;
                    break;
                }
            }
            if (k == 0)
                books.add(newBook);
        }
    }

    public void deleteBook(String nameBook) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getNameBook().equals(nameBook))
                if (books.get(i).getIdentifierLendBook() == null)
                    books.remove(books.get(i));
        }
    }

    public Book searchBookName(String nameBook) {
        int k = -1;
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getNameBook().equals(nameBook)) {
                k = i;
                break;
            }
        }
        return books.get(k);
    }

    public ArrayList<Book> searchBookAuthor(String nameAuthor) {
        ArrayList<Book> newBooks = new ArrayList<>();
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getNameAuthor().equals(nameAuthor)) {
                newBooks.add(books.get(i));
            }
        }
        return newBooks;
    }

    public void lendBook(Visitor visitor, String nameBook) {
        for (int i = 0; i < books.size(); i++) {
            if ((books.get(i).getNameBook().equals(nameBook)) &&
                    visitor.getIdentifierLendVisitor() &&
                    (books.get(i).getIdentifierLendBook() == null)) {
                if (visitor.getIdentifierVisitor() == null)
                    visitor.setIdentifierVisitor(visitor.getNameVisitor() + visitor.numberVisitor++);
                books.get(i).setIdentifierLendBook(visitor.getIdentifierVisitor());
                visitor.setIdentifierLendVisitor(false);
            }
        }
    }

    public void returnBook(Visitor visitor, Book book, double grade) {
        if (book.getIdentifierLendBook().equals(visitor.getIdentifierVisitor())) {
            book.setIdentifierLendBook(null);
            visitor.setIdentifierLendVisitor(true);
            book.setCountTake(book.getCountTake() + 1);
            book.getGradesBook().add(grade);
        }
    }

    public double gradeAvgBook(String nameBook) {
        int index = 0;
        double sum = 0;
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getNameBook().equals(nameBook)) {
                index = i;
            }
        }
        for (int i = 0; i < books.get(index).getGradesBook().size(); i++) {
            sum += books.get(index).getGradesBook().get(i);
        }
        books.get(index).setGradeBook(sum / books.get(index).getCountTake());
        return books.get(index).getGradeBook();
    }
}

