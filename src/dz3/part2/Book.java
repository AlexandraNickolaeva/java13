package dz3.part2;

import java.util.ArrayList;

public class Book {
    private String nameBook;
    private String nameAuthor;
    private String identifierLendBook; // null - не одолжена or идентификатор одолжившего посетителя
    private int countTake = 0; // кол-во возвращений книги
    private double gradeBook; // оценка книги
    private ArrayList<Double> gradesBook = new ArrayList<>(); // все оценки книги

    Book(String nameBook, String nameAuthor) {
        this.nameBook = nameBook;
        this.nameAuthor = nameAuthor;
    }

    public String getNameBook() {
        return nameBook;
    }

    public void setNameBook(String newNameBook) {
        nameBook = newNameBook;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setNameAuthor(String newNameAuthor) {
        nameAuthor = newNameAuthor;
    }

    public String getIdentifierLendBook() {
        return identifierLendBook;
    }

    public void setIdentifierLendBook(String newIdentifierLendBook) {
        identifierLendBook = newIdentifierLendBook;
    }

    public int getCountTake() {
        return countTake;
    }

    public void setCountTake(int newCountTake) {
        countTake = newCountTake;
    }

    public double getGradeBook() {
        return gradeBook;
    }

    public void setGradeBook(double newGradeBook) {
        gradeBook = newGradeBook;
    }

    public ArrayList<Double> getGradesBook() {
        return gradesBook;
    }

    public void setGradesBook(ArrayList<Double> newGradesBook) {
        gradesBook = newGradesBook;
    }

    @Override
    public String toString() {
        return nameBook + "-" + nameAuthor;
    }
}

