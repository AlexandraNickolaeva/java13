package dz3.part2;

public class Main {
    public static void main(String[] args) {
        Library library1 = new Library();
        Book book1 = new Book("Яма", "Куприн");
        Book book2 = new Book("Обломов", "Гончаров");
        Book book3 = new Book("Анна Каренина", "Толстой");
        Book book4 = new Book("Война и мир", "Толстой");
        Book book5 = new Book("Гранатовый браслет", "Куприн");
        Book book6 = new Book("Гранатовый браслет", "К");

        // 1. Добавляем книги в библиотеку:
        library1.addBook(book1);
        library1.addBook(book2);
        library1.addBook(book3);
        library1.addBook(book4);
        library1.addBook(book5);
        // Проверяем добавленные книги:
        System.out.println(library1.books);
        // Пробуем добавить книгу с названием, которое уже есть в библиотеке:
        library1.addBook(book6);
        // Проверяем и наблюдаем, что данная книга не добавилась в библиотеку:
        System.out.println(library1.books);

        // 3. Найти и вернуть книгу по названию:
        System.out.println(library1.searchBookName("Война и мир"));

        // 4. Найти и вернуть список книг по автору:
        System.out.println(library1.searchBookAuthor("Куприн"));
        System.out.println(library1.searchBookAuthor("Гончаров"));
        System.out.println(library1.searchBookAuthor("Толстой"));

        Visitor visitor1 = new Visitor("Виктор");
        Visitor visitor2 = new Visitor("Меркурий");
        Visitor visitor3 = new Visitor("Гарфилд");
        Visitor visitor4 = new Visitor("Клевер");

        // 5. Одалживаем книгу посетителю:
        library1.lendBook(visitor1, "Яма");
        // Идентификатор посетителя:
        System.out.println(visitor1.getIdentifierVisitor());
        // Идентификатор книги, равный идентификатору посетителя ее одолживший:
        System.out.println(book1.getIdentifierLendBook());
        // Идентификатор посетителя = false, обозначающий, что он - должник
        System.out.println(visitor1.getIdentifierLendVisitor());

        // Пробуем одолжить еще одну книгу должнику
        library1.lendBook(visitor1, "Анна Каренина");
        // Наблюдаем, что идентификатор книги остался null:
        System.out.println(book3.getIdentifierLendBook());

        // 2. Удалить книгу из библиотеки, если она не одолжена:
        library1.deleteBook("Яма"); // одолжена
        library1.deleteBook("Война и мир"); // не одолжена
        // Проверяем книги:
        System.out.println(library1.books);

        //6. Вернуть книгу в библиотеку:
        // От другого посетителя:
        library1.returnBook(visitor2, book1, 9);
        System.out.println(book1.getIdentifierLendBook()); // идентификатор книги не null, значит, одолжена
        System.out.println(visitor1.getIdentifierLendVisitor()); // идентификатор посетителя = false, значит, должник
        // От нужного посетителя:
        library1.returnBook(visitor1, book1, 3);
        System.out.println(book1.getIdentifierLendBook()); // идентификатор книги null, значит, не одолжена
        System.out.println(visitor1.getIdentifierLendVisitor()); // идентификатор посетителя true, значит не должник

        // Снова одолжим эту же книгу, но другому посетителю:
        library1.lendBook(visitor2, "Яма");
        System.out.println(book1.getIdentifierLendBook()); // идентификатор книги не null, значит, одолжена
        System.out.println(visitor2.getIdentifierLendVisitor()); // идентификатор посетителя = false, значит, должник
        library1.returnBook(visitor2, book1, 5);

        // 8. Оценка книги по её именованию:

        library1.lendBook(visitor2, "Гранатовый браслет");
        library1.returnBook(visitor2, book5, 10);
        library1.lendBook(visitor2, "Гранатовый браслет");
        library1.returnBook(visitor2, book5, 4);
        library1.lendBook(visitor1, "Гранатовый браслет");
        library1.returnBook(visitor1, book5, 1);

        library1.lendBook(visitor3, "Анна Каренина");
        library1.returnBook(visitor3, book3, 6);
        library1.lendBook(visitor4, "Анна Каренина");
        library1.returnBook(visitor4, book3, 5);

        System.out.println(book5 + " " + book5.getGradesBook() + " " + book5.getCountTake());
        System.out.println(library1.gradeAvgBook("Гранатовый браслет"));

        System.out.println(book3 + " " + book3.getGradesBook() + " " + book3.getCountTake());
        System.out.println(library1.gradeAvgBook("Анна Каренина"));

        System.out.println(book2 + " " + book2.getGradesBook() + " " + book2.getCountTake());
        System.out.println(library1.gradeAvgBook("Обломов"));
    }
}
