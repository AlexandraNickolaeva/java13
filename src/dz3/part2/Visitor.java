package dz3.part2;

public class Visitor {
    private String nameVisitor;
    private String identifierVisitor;
    private boolean identifierLendVisitor = true; // true - не должник or false - должник
    static int numberVisitor = 0;

    Visitor(String nameVisitor) {
        this.nameVisitor = nameVisitor;
    }

    public String getNameVisitor() {
        return nameVisitor;
    }

    public void setNameVisitor(String newNameVisitor) {
        nameVisitor = newNameVisitor;
    }

    public String getIdentifierVisitor() {
        return identifierVisitor;
    }

    public void setIdentifierVisitor(String newIdentifierVisitor) {
        identifierVisitor = newIdentifierVisitor;
    }

    public boolean getIdentifierLendVisitor() {
        return identifierLendVisitor;
    }

    public void setIdentifierLendVisitor(boolean newIdentifierLendVisitor) {
        identifierLendVisitor = newIdentifierLendVisitor;
    }
}