package HomeWork3_1;
//[JAVA13][ДЗ№3_1] Задача 3 от NIKOLAEVA-AD259
import java.util.Arrays;

public class StudentService {

    public Student bestStudent(Student students[]){
        int indexMax = 0;
        double max = students[0].avgGrades();
        for(int i = 1; i<students.length; i++){
            if(students[i].avgGrades()>max){
                max = students[i].avgGrades();
                indexMax = i;
            }
        }
        return students[indexMax];
    }

    public void sortBySurname(Student students[]){
        String[] surname = new String[students.length];
        for(int i = 0; i<students.length; i++){
            surname[i] = students[i].getSurname();
        }

        Arrays.sort(surname);

        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length; j++) {
                if(surname[i]==students[j].getSurname()){
                    Student temp = students[i];
                    students[i] = students[j];
                    students[j] = temp;
                }
            }
        }
    }
}
