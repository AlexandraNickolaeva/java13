package DZ_3.Part_1;
//[JAVA13][ДЗ№3_1] Задача 7 от NIKOLAEVA-AD259
public class TriangleChecker {
    private double a;
    private double b;
    private double c;

    public static boolean isCheck(double a, double b, double c) {
        return (a < (b + c) && b < (a + c) && c < (a + b));
    }

    public static void main(String[] args) {
        TriangleChecker test = new TriangleChecker();
        System.out.println(test.isCheck(1, 2, 3));
        System.out.println(test.isCheck(6, 2, 3));
        System.out.println(test.isCheck(1.75, 2, 3));
    }
}
