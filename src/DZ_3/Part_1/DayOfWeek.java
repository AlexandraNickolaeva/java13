package DZ_3.Part_1;
//[JAVA13][ДЗ№3_1] Задача 5 от NIKOLAEVA-AD259
public class DayOfWeek {
    private byte numberWeek;
    private String dayWeek;

    DayOfWeek(byte numberWeek, String dayWeek) {
        this.numberWeek = numberWeek;
        this.dayWeek = dayWeek;
    }

    @Override
    public String toString() {
        return numberWeek + " " + dayWeek;
    }
}

class DayOfWeekMain {
    public static void main(String[] args) {
        DayOfWeek[] week = new DayOfWeek[7];
        week[0] = new DayOfWeek((byte) 1, "Monday");
        week[1] = new DayOfWeek((byte) 2, "Tuesday");
        week[2] = new DayOfWeek((byte) 3, "Wednesday");
        week[3] = new DayOfWeek((byte) 4, "Thursday");
        week[4] = new DayOfWeek((byte) 5, "Friday");
        week[5] = new DayOfWeek((byte) 6, "Saturday");
        week[6] = new DayOfWeek((byte) 7, "Sunday");

        for (int i = 0; i < week.length; i++) {
            System.out.println(week[i]);
        }
    }
}

