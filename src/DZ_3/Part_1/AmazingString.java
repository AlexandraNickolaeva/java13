package DZ_3.Part_1;
//[JAVA13][ДЗ№3_1] Задача 6 от NIKOLAEVA-AD259
public class AmazingString {
    private char[] arrChar;

    AmazingString(char[] arrChar) {
        this.arrChar = arrChar;
    }

    AmazingString(String s) {
        this.arrChar = s.toCharArray();
    }

    public char iSymbol(int i) { // вернуть i-ый символ строки
        return arrChar[i];
    }

    public int lenthtChar() { // вернуть длину строки
        return arrChar.length;
    }

    public void screenChar() { // вывести строку на экран
        for (int i = 0; i < arrChar.length; i++)
            System.out.print(arrChar[i]);
    }

    public boolean isSubArrChar(char[] arr) { // проверить, есть ли переданная подстрака (массив)
        boolean answer = false;
        for (int i = 0; i < arrChar.length; i++) {
            for (int j = 0; j < arr.length && i < arrChar.length - j; j++) {
                if (arr[j] == arrChar[i + j]) {
                    answer = true;
                } else {
                    answer = false;
                    break;
                }
            }
            if (answer == true)
                break;
        }
        return answer;
    }

    public boolean isSubString(String a) { // проверить, есть ли переданная подстрака (строка)
        boolean answer = false;
        String t = "";

        for (int i = 0; i < arrChar.length; i++) {
            for (int j = i; j < (a.length() + i) && j < arrChar.length; j++) {
                t += "" + arrChar[j];
            }
            answer = a.equals(t);
            if (answer == true)
                break;
            else
                t = "";
        }
        return answer;
    }

    public void deleteEmpty() { // удалить из строки ведущие пробельные символы
        int count = 0;
        for (int i = 0; i < arrChar.length; i++) {
            if (arrChar[i] == ' ')
                count++;
        }
        char[] newChar = new char[arrChar.length - count];
        for (int i = 0, j = 0; i < arrChar.length; i++) {
            if (arrChar[i] != ' ') {
                newChar[j] = arrChar[i];
                j++;
            }
        }
        this.arrChar = newChar;
    }

    public void coup() { // развернуть строку
        for (int i = 0, j = arrChar.length - 1; i < arrChar.length / 2; i++, j--) {
            char temp = arrChar[i];
            arrChar[i] = arrChar[j];
            arrChar[j] = temp;
        }
    }
}
